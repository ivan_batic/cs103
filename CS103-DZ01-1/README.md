# CS103 - DZ01-1 - Divisability by 11

Checking if an integer can be divided by 11.

## Sample Output:
>Enter a number: 532  
>532 is not divisible by 11  
  
>Enter a number: 121  
>121 is divisible by 11  