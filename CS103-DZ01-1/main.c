#include <stdio.h>

short unsigned int isDivisible(int number, int by) {
    return number % by == 0;
}

int main() {

    int unsigned inputNumber;
    printf("Enter a number: ");
    scanf("%i", &inputNumber);
    printf("%i is %s by 11\n", inputNumber, isDivisible(inputNumber, 11) ? "divisible" : "not divisible");

    return 0;
}