#include <stdio.h>

void swap(int *nums, int index1, int index2) {
    int tmp = nums[index1];
    nums[index1] = nums[index2];
    nums[index2] = tmp;
}

void printNumbers(const int *nums, FILE *outputFp) {

    for (int i = 0; nums[i] != NULL; i++) {
        fputc(nums[i] + '0', outputFp);
    }
    fputc('\n', outputFp);
}

void permutate(int nums[], int n, FILE *outputFp) {
    if (n == 1) {
        printNumbers(nums, outputFp);
        return;
    }

    for (int i = 0; i < n - 1; i++) {
        permutate(nums, n - 1, outputFp);
        if (n % 2 == 0) {
            swap(nums, i, n - 1);
        } else {
            swap(nums, 0, n - 1);
        }
    }
    permutate(nums, n - 1, outputFp);
}


int main(int argc, char *argv[]) {
    int numbers[] = {1, 2, 3, 4, 5, NULL};

    const char *filePath = "permutacije.txt";
    if(argc == 2) filePath = argv[1];

    FILE *fp = fopen(filePath, "w");
    permutate(numbers, 5, fp);
    fclose(fp);

    return 0;
}

