#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define TRUE 1
#define FALSE 0

char *toLowerCase(char *input, int length) {
    char *output = malloc(length);
    for (int i = 0; i < length; i++) {
        output[i] = input[i];
        if ((input[i] >= 65) && (input[i] <= 90)) {
            output[i] = input[i] | 32;
        }
    }

    return output;
}

char *bubbleSortString(char *input, int length) {

    int flipCount = 0;
    char *str = strdup(input);

    do {
        flipCount = 0;
        for (int index = 0; index < length - 1; index++) {
            // If the next letter is larger, swap them
            if (str[index] > str[index + 1]) {
                char tmp = str[index];
                str[index] = str[index + 1];
                str[index + 1] = tmp;
                flipCount++;
            }
        }
    } while (flipCount != 0);
    return str;
}

int isAnagram(int firstLength, char *first, int secondLength, char *second) {
    if (firstLength != secondLength) {
        return FALSE;
    }

    char *orderedFirst = bubbleSortString(first, firstLength);
    char *orderedSecond = bubbleSortString(second, secondLength);

    int areAnagrams = strcmp(orderedFirst, orderedSecond) == 0;

    free(orderedFirst);
    free(orderedSecond);

    return areAnagrams;
}

int main(int argc, char **argv) {

    if (argc != 3) {
        printf("Usage:\n\t%s <first word> <second word>\n", argv[0]);
        return 1;
    }

    char *firstStr = argv[1];
    char *secondStr = argv[2];

    int firstLen = strlen(firstStr);
    int secondLen = strlen(secondStr);

    char *firstLowered = toLowerCase(firstStr, firstLen);
    char *secondLowered = toLowerCase(secondStr, secondLen);

    int stringsAreAnagrams = isAnagram(firstLen, firstLowered, secondLen, secondLowered);


    printf("Are %s and %s anagrams?\n%s\n\n", firstStr, secondStr, stringsAreAnagrams ? "Yes" : "No");

    free(firstLowered);
    free(secondLowered);

    return 0;
}