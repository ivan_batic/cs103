# CS103 - DZ01-2 Description 

The program generates an array of 15 random numbers and stores them into the **inputNumbers** array of **INPUT_SIZE** values.  
Hex values are calculated on the fly by using the **decToHex** function and printed out immediately.  
The conversion function needs a buffer pointer to which it will store a result, so we don't have to use the heap.


## Sample Output:
![Program Output](http://i.imgur.com/jiyf42E.png?1)
