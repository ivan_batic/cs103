#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MAX_HEX_LENGTH 50
#define INPUT_SIZE 15

/**
 * Convert a decimal number into it's hex representation
 */
void decToHex(int dec, char *buffer) {

    // Set hex storage
    char reversedHex[MAX_HEX_LENGTH];

    // Temporary calculation vars
    int hexCounter = 0;
    int remainder;

    // Divide decimal input by 16 until the quotient is 0
    // Store remainders in the process
    do {
        remainder = dec % 16;
        dec /= 16;
        reversedHex[hexCounter++] = (char) (remainder < 10 ? remainder + 48 : remainder + 55);
    } while (dec != 0);


    // Reverse the hex
    for (int i = hexCounter - 1; i >= 0; i--) {
        int bufferIndex = hexCounter - 1 - i;
        buffer[bufferIndex] = reversedHex[i];
    }
    // Mark the end of the string
    buffer[hexCounter] = '\0';
}

int getRandom(int min, int max) {
    return rand() % (max - min) + min;
}

int main() {
    // Seed the randomizer
    srand(time(NULL));
    // Declare an array for the input numbers
    int inputNumbers[INPUT_SIZE];
    // Temporary value for the inputs
    int randomValueTmp;
    // Temporary value for the hex
    char hexTmp[MAX_HEX_LENGTH];

    // Print out the table heading
    printf("%10s%10s\n", "Decimal", "Hex");
    printf("%.*s\n", 20, "------------------------");

    // Go through each position (0-INPUT_SIZE), generate a random value store it and print the corresponding HEX
    for (int i = 0; i < INPUT_SIZE; i++) {
        randomValueTmp = getRandom(0, 10000);
        inputNumbers[i] = randomValueTmp;
        decToHex(randomValueTmp, hexTmp);
        printf("%10i%10s\n", randomValueTmp, hexTmp);
    }

    return 0;
}