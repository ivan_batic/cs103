#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * Corresponds to “firma”
 * Naming the struct like that would be incorrect and misleading since the file entry represents a
 * particular contract with the company, and not the company itself.
 */
typedef struct Contract {
    char companyName[255];
    float contractValue;
} Contract;

typedef struct ContractSum {
    char companyName[255];
    float totalValue;
} ContractSum;

char *readLine(const FILE *fileHandle, const char *line);

/**
 * Corresponds to “Ugovori”
 *
 * Naming the function like that would be a textbook example of bad code since
 * it's intention would be completely obfuscated and misleading.
 */
float findContractValueSumByCompanyName(char *name, ContractSum *list) {

    int counter = 0;
    while (list[counter].companyName[0] != '\0') {
        if (strcmp(list[counter].companyName, name) == 0) {
            return list[counter].totalValue;
        }
        counter++;
    }
    return -1;
}

int main(int argc, char *argv[]) {

    if (argc < 2) {
        printf("Usage:\n\t%s <input_path>\n", argv[0]);
        return 1;
    }
    // Input file path on disk
    const char *inputPath = argv[1];

    // File stream
    FILE *fileHandle = fopen(inputPath, "r");
    // Return "No such file or directory" error code if the file does not exist
    if (fileHandle == NULL) return 2;

    // Temporary values for storing parsed input
    int contractsCount, counter;
    char line[256];

    // Fetch the first line and initialize an array of contracts according to the number of contracts we read from it
    readLine(fileHandle, line);
    sscanf(line, "%d\n", &contractsCount);
    counter = contractsCount;

    // Initialize the contract array
    Contract *contracts = malloc(contractsCount * sizeof(Contract));
    ContractSum *contractSumList = malloc(contractsCount * sizeof(ContractSum));
    int contractSumLength = 0;


    // Go through all pending entries and parse them into contract structs
    while (--counter >= 0 && readLine(fileHandle, line)) {
        sscanf(line, "%[^\,],%f\n", &contracts[counter].companyName, &contracts[counter].contractValue);

        // Check currently available sums
        for (int i = 0; i < contractsCount; i++) {

            if (strcmp(contractSumList[i].companyName, contracts[counter].companyName) == 0) {
                // If current element has the same company name, add up the values
                contractSumList[i].totalValue += contracts[counter].contractValue;
                break;
            } else if (contractSumList[i].companyName[0] == '\0') {
                // If we've reached the end and haven't found the company, add it
                strcpy(&contractSumList[i].companyName, contracts[counter].companyName);
                contractSumList[i].totalValue = contracts[counter].contractValue;
                contractSumLength++;
                break;
            }
        }
    }

    // Show a table of contract sums
    for (int i = 0; i < contractSumLength; i++) {
        printf("%-30s %.2f\n",
               contractSumList[i].companyName,
               findContractValueSumByCompanyName(contractSumList[i].companyName, contractSumList)
        );
    }

    // Cleanup
    fclose(fileHandle);
    free(contracts);
    free(contractSumList);


    return 0;
}

char *readLine(const FILE *fileHandle, const char *line) {
    return fgets(line, 256, fileHandle);
}



