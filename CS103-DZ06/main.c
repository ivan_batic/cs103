#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "DoublyLinkedList.h"

struct NegativeSliceData *extractLargestNegativeListInfo(DoublyLinkedList *list);

struct NegativeSliceData *extractFirstNegativeListInfo(DoublyLinkedList *list);

typedef struct NegativeSliceData {
    int firstNodeIndex;
    int sliceLength;
} NegativeSliceData;

int main(int argc, char **argv) {

    DoublyLinkedList *mainList = createDoublyLinkedList();

    // Insert elements into the main list
    for (int i = 1; i < argc; i++) {
        int tmp;
        sscanf(argv[i], "%x", &tmp);
        append(tmp, mainList);
    }

    printf("Main List:\n");
    printElements(mainList);

    // Extract the first negative sublist
    NegativeSliceData *sliceData = extractFirstNegativeListInfo(mainList);
    DoublyLinkedList *sublist = sliceFromIndex(mainList, sliceData->firstNodeIndex, sliceData->sliceLength);

    // Print it
    printf("\n\nThe Largest Negative Sub-List in Reverse:\n");
    printReverse(sublist);

    // Cleanup
    removeList(mainList);
    removeList(sublist);
    free(sliceData);

    return 0;
}


/**
 * I misread the assignment and extracted the *largest* negative subset here instead of the first one
 * Now I'm too emotionally attached to the code to delete this.
 */
struct NegativeSliceData *extractLargestNegativeListInfo(DoublyLinkedList *list) {
    NegativeSliceData *data = malloc(sizeof(NegativeSliceData));

    Node *current = list->_anchor;

    int isCountingActive = false;
    int currentIndex = 0;

    int largestCountSoFar = 0;
    int largestCountSoFarIndex = 0;

    int currentCount = 0;
    int currentCountIndex = 0;


    while (current->next != NULL) {
        current = current->next;

        // If we're on a negative number
        if (current->data < 0) {
            // If this is a first negative number in series
            // start a new series
            if (!isCountingActive) {
                isCountingActive = true;
                currentCount = 1;
                currentCountIndex = currentIndex;
            } else {
                // If this is not the first number in a series, add up to
                // the existing one
                currentCount++;
            }
        } else {
            // If we are on a positive number, it can be a positive series
            // or a negative series breaker

            if (isCountingActive) {
                // If it's a negative series breaker, handle the breaking
                isCountingActive = false;

                // Check if the current counting series is the best so far,
                // and act upon it
                if (currentCount > largestCountSoFar) {
                    largestCountSoFar = currentCount;
                    largestCountSoFarIndex = currentCountIndex;
                }

            }
            // If it's an element of a positive series, nothing needs to be done
        }

        // Maybe our largest series ends at the end of the list, so check it out once more
        if (currentCount > largestCountSoFar) {
            largestCountSoFar = currentCount;
            largestCountSoFarIndex = currentCountIndex;
        }

        currentIndex++;
    }

    data->sliceLength = largestCountSoFar;
    data->firstNodeIndex = largestCountSoFarIndex;
    return data;
}

struct NegativeSliceData *extractFirstNegativeListInfo(DoublyLinkedList *list) {
    NegativeSliceData *data = malloc(sizeof(NegativeSliceData));

    Node *current = list->_anchor;

    int isCountingActive = false;
    int currentIndex = 0;

    int currentCount = 0;
    int currentCountIndex = 0;


    while (current->next != NULL) {
        current = current->next;

        // If we're on a negative number
        if (current->data < 0) {
            // If this is a first negative number in series
            // start a new series
            if (!isCountingActive) {
                isCountingActive = true;
                currentCount++;
                currentCountIndex = currentIndex;
            } else {
                // If this is not the first number in a series, add up to
                // the existing one
                currentCount++;
            }
        } else {
            // If we are on a positive number, it can be a positive series
            // or a negative series breaker

            if (isCountingActive) {

                // Check if the current counting series is the best so far,
                // and act upon it
                data->sliceLength = currentCount;
                data->firstNodeIndex = currentCountIndex;
                // If it's a negative series breaker, handle the breaking
                return data;
            }
            // If it's an element of a positive series, nothing needs to be done
        }

        currentIndex++;
    }

    data->sliceLength = 0;
    data->firstNodeIndex = 0;
    return data;
}