#ifndef CS103_DZ06_DOUBLYLINKEDLIST_H
#define CS103_DZ06_DOUBLYLINKEDLIST_H

#endif //CS103_DZ06_DOUBLYLINKEDLIST_H

/**
 * Types
 * ---------------------------------------------------------------------------------------------------------------------
 */

typedef struct Node {
    int data;
    struct Node *next;
    struct Node *prev;
} Node;

typedef struct DoublyLinkedList {
    Node *head;
    Node *tail;
    int length;

    Node *_anchor;
} DoublyLinkedList;

/**
 * Definitions
 * ---------------------------------------------------------------------------------------------------------------------
 */
Node *createNode(int data);

DoublyLinkedList *createDoublyLinkedList();

Node *_addFirst(Node *node, DoublyLinkedList *list);

Node *append(int data, DoublyLinkedList *list);

Node *prepend(int data, DoublyLinkedList *list);

Node *insertAfter(int data, Node *baseNode, DoublyLinkedList *list);

Node *insertBefore(int data, Node *baseNode, DoublyLinkedList *list);

DoublyLinkedList *sliceFromNode(Node *startingNode, int endIndex);

DoublyLinkedList *sliceFromIndex(DoublyLinkedList *list, int start, int elements);

void setTail(Node *node, DoublyLinkedList *list);

void setHead(Node *node, DoublyLinkedList *list);

void removeNode(Node *node, DoublyLinkedList *list);

void removeAllNodes(DoublyLinkedList *list);

void removeList(DoublyLinkedList *list);

void printElements(DoublyLinkedList *list);

void printReverse(DoublyLinkedList *list);

