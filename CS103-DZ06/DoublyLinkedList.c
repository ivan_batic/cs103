#include <stdio.h>
#include <stdlib.h>
#include "DoublyLinkedList.h"


Node *createNode(int data) {
    Node *node = (Node *) malloc(sizeof(Node));
    node->data = data;
    node->next = NULL;
    node->prev = NULL;
    return node;
}

DoublyLinkedList *createDoublyLinkedList() {
    DoublyLinkedList *list = malloc(sizeof(DoublyLinkedList));
    list->head = NULL;
    list->tail = NULL;
    list->length = 0;

    list->_anchor = createNode(0);

    return list;
}

void setHead(Node *node, DoublyLinkedList *list) {
    list->head = node;
    list->_anchor->next = node;
}

void setTail(Node *node, DoublyLinkedList *list) {
    list->tail = node;
    list->_anchor->prev = node;
}

Node *_addFirst(Node *node, DoublyLinkedList *list) {
    setHead(node, list);
    setTail(node, list);
    return node;
}


Node *append(int data, DoublyLinkedList *list) {
    Node *node = createNode(data);
    list->length++;

    if (list->length == 1) {
        return _addFirst(node, list);
    }

    node->prev = list->tail;
    list->tail->next = node;
    list->tail = node;
    list->_anchor->prev = node;

    return node;
}


Node *prepend(int data, DoublyLinkedList *list) {
    Node *node = createNode(data);
    list->length++;

    if (list->length == 1) {
        return _addFirst(node, list);
    }

    node->next = list->head;
    list->head->prev = node;
    list->_anchor->next = node;
    list->head = node;

    return node;
}

DoublyLinkedList *sliceFromNode(Node *startingNode, int elements) {

    DoublyLinkedList *list = createDoublyLinkedList();
    if(elements == 0){
        return list;
    }

    int itemsFoundCount = 1;

    Node * firstNode = append(startingNode->data, list);
//    setHead(firstNode, list);
    Node *current = startingNode;


    while (current->next != NULL && itemsFoundCount < elements) {

        current = current->next;
        append(current->data, list);
        itemsFoundCount++;
    }

//    setTail(current, list);

    return list;
}


DoublyLinkedList *sliceFromIndex(DoublyLinkedList *list, int start, int elements) {

    if (start < 0) {
        exit(1);
    }

    Node *currentNode = list->_anchor;
    int currentIndex = -1;
    while (currentNode->next != NULL && currentIndex < start) {
        currentNode = currentNode->next;
        currentIndex++;
    }

    if (currentIndex == start) {
        return sliceFromNode(currentNode, elements);
    }
    return createDoublyLinkedList();
}


void printElements(DoublyLinkedList *list) {
    if (list->length == 0) {
        printf("The list is empty.\n");
        return;
    }

    Node *current = list->_anchor;

    while (current->next != NULL) {
        current = current->next;
        printf("%d ", current->data);
    }
}

void printReverse(DoublyLinkedList *list) {
    if (list->length == 0) {
        printf("The list is empty.\n");
        return;
    }

    Node *current = list->_anchor;
    while (current->prev != NULL) {
        current = current->prev;
        printf("%d ", current->data);
    }
}

Node *insertAfter(int data, Node *baseNode, DoublyLinkedList *list) {
    Node *newNode = createNode(data);
    newNode->next = baseNode->next;
    newNode->prev = baseNode;

    if (newNode->next == NULL) {
        setTail(newNode, list);
    } else {
        baseNode->next->prev = newNode;
    }
    baseNode->next = newNode;

    list->length++;

    return newNode;
}

Node *insertBefore(int data, Node *baseNode, DoublyLinkedList *list) {
    Node *newNode = createNode(data);
    newNode->next = baseNode;
    newNode->prev = baseNode->prev;

    if (newNode->prev == NULL) {
        setHead(newNode, list);
    } else {
        baseNode->prev->next = newNode;
    }

    baseNode->prev = newNode;

    list->length++;

    return newNode;
}

void removeNode(Node *node, DoublyLinkedList *list) {
    if (node->prev != NULL) {
        node->prev->next = node->next;
    } else {
        setHead(node->next, list);
    }

    if (node->next != NULL) {
        node->next->prev = node->prev;
    } else {
        setTail(node->prev, list);
    }

    free(node);

    list->length--;
}

void removeAllNodes(DoublyLinkedList *list) {

    Node *current = list->_anchor;

    while (current != NULL && current->next != NULL) {
        current = current->next;
        Node *next = current->next;
        free(current);
        current = next;
    }

    list->length = 0;
    setHead(NULL, list);
    setTail(NULL, list);
}

void removeList(DoublyLinkedList * list){
    removeAllNodes(list);
    free(list);
}