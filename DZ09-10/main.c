#include <stdlib.h>
#include <stdio.h>
#include "bst.h"

void _reduce_keys_lower_than(BST_NODE *node, int varargc, va_list valist) {
    int upper_bound = va_arg(valist, int);
    int *counter = va_arg(valist, int*);

    if (node->key < upper_bound) {
        printf("Counting in Node: %d\n", node->key);
        *counter = *counter + 1;
    }
}

int get_count_of_nodes_with_keys_smaller_than_value(BST *bst, int upper_bound) {

    int *counter = malloc(sizeof(int));
    *counter = 0;

    bst_reduce(bst->root_node, _reduce_keys_lower_than, 2, upper_bound, counter);

    int result = *counter;
    free(counter);

    return result;
}


void _reduce_into_non_leaves(BST_NODE *node, int varargc, va_list valist) {

    if (!bst_is_leaf(node)) {
        printf("Found a non-leaf: %d\n", node->key);
        int *counter = va_arg(valist, int*);
        *counter += 1;
    }
}

int count_non_leaves_in_bst(BST *bst) {
    int *counter = malloc(sizeof(int));
    *counter = 0;

    bst_reduce(bst->root_node, _reduce_into_non_leaves, 1, counter);

    int result = *counter;
    free(counter);

    return result;
}

int main() {

    BST *bst = bst_create();

    bst_insert(bst, 5);
    bst_insert(bst, 4);
    bst_insert(bst, 9);
    bst_insert(bst, 2);
    bst_insert(bst, 7);
    bst_insert(bst, 3);
    bst_insert(bst, 40);
    bst_insert(bst, 52);
    bst_insert(bst, 21);
    bst_insert(bst, 33);
    bst_insert(bst, 19);

    // DZ09
    int upper_bound = 25;
    printf("DZ09 - nodes with keys lower than %d...\n", upper_bound);
    int sum = get_count_of_nodes_with_keys_smaller_than_value(bst, upper_bound);
    printf("Total elements lower than %d: %d\n\n\n", upper_bound, sum);

    // DZ10
    printf("DZ10 - Looking for non-leaves...\n");
    int non_leaves_count = count_non_leaves_in_bst(bst);
    printf("Total: %d\n", non_leaves_count);

    bst_destroy(bst);
    return 0;
}