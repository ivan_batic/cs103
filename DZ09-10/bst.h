#include <stdarg.h>

#ifndef DZ09_10_BST_H
#define DZ09_10_BST_H

#endif //DZ09_10_BST_H

typedef struct BST_NODE {
    int key;
    char *value;
    struct BST_NODE *left_child;
    struct BST_NODE *right_child;
    int node_count;
} BST_NODE;

typedef struct BST {
    struct BST_NODE *root_node;

} BST;

void bst_reduce(BST_NODE *current_node, void (*reducer)(BST_NODE *, int, va_list), int va_cnt, ...);

BST *bst_create();

BST_NODE *bst_insert(BST *bst, int key);

int bst_is_leaf(BST_NODE *node);

int bst_get_non_leaf_count(BST_NODE *root);

void bst_destroy(BST *bst);