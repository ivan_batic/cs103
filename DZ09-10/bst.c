#include <stdlib.h>
#include "bst.h"
#include <stdarg.h>

BST *bst_create() {
    BST *result = malloc(sizeof(BST));
    result->root_node = NULL;
    return result;
}

BST_NODE *bst_create_node(int key) {
    BST_NODE *node = malloc(sizeof(BST_NODE));
    node->key = key;
    node->left_child = NULL;
    node->right_child = NULL;

    return node;
}


void bst_insert_node(BST_NODE *root_node, BST_NODE *new_node) {
    if (new_node->key <= root_node->key) {
        if (root_node->left_child == NULL) {
            root_node->left_child = new_node;
        } else {
            bst_insert_node(root_node->left_child, new_node);
        }
    } else {
        if (root_node->right_child == NULL) {
            root_node->right_child = new_node;
        } else {
            bst_insert_node(root_node->right_child, new_node);
        }
    }
}

void _bst_reduce_with_valist(BST_NODE *current_node,
                             void (*reducer)(BST_NODE *, int, va_list *),
                             int va_cnt,
                             va_list valist) {

    if (current_node == NULL) {
        return;
    }
    va_list valist_left;
    va_list valist_right;

    va_copy(valist_left, valist);
    va_copy(valist_right, valist);

    reducer(current_node, va_cnt, valist);

    _bst_reduce_with_valist(current_node->left_child, reducer, va_cnt, valist_left);
    _bst_reduce_with_valist(current_node->right_child, reducer, va_cnt, valist_right);

    va_end(valist_left);
    va_end(valist_right);

}

void bst_reduce(BST_NODE *current_node, void (*reducer)(BST_NODE *, int, va_list), int va_cnt, ...) {
    if (current_node == NULL) {
        return;
    }
    va_list valist;
    va_list valist_left;
    va_list valist_right;

    va_start(valist, va_cnt);

    va_copy(valist_left, valist);
    va_copy(valist_right, valist);


    reducer(current_node, va_cnt, valist);

    _bst_reduce_with_valist(current_node->left_child, reducer, va_cnt, valist_left);
    _bst_reduce_with_valist(current_node->right_child, reducer, va_cnt, valist_right);

    va_end(valist);
    va_end(valist_left);
    va_end(valist_right);
}


BST_NODE *bst_insert(BST *bst, int key) {
    BST_NODE *node = bst_create_node(key);

    if (bst->root_node == NULL) {
        bst->root_node = node;
    } else {
        bst_insert_node(bst->root_node, node);
    }

    return node;
}

int bst_is_leaf(BST_NODE *node) {
    return !node->left_child && !node->right_child;
}

void _bst_destroy_node(BST_NODE *node) {
    free(node);
}

void bst_destroy(BST *bst) {
    bst_reduce(bst->root_node, _bst_destroy_node, 0);
    free(bst);
}

