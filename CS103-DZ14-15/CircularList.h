//
// Created by Ivan Batic on 1/23/16.
//

#ifndef CS103_DZ14_15_CIRCULARLIST_H
#define CS103_DZ14_15_CIRCULARLIST_H

#endif //CS103_DZ14_15_CIRCULARLIST_H


typedef struct Node {
    char *name;
    struct Node *_next;
} Node;

typedef struct CList {
    int length;
    Node *_head;
    Node *_tail;
} CList;

Node *createNode(char *name);

CList *createCircularList();

Node *append(char *name, CList *list);

void removeNode(Node *node, CList *list);

int getSize(CList *list);

int isEmpty(CList *list);

Node * getHead(CList *list);

void destroyNode(Node * node);

void destroyList(CList * list);