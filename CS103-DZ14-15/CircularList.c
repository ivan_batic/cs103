#include <stdlib.h>
#include "CircularList.h"

Node *createNode(char *name) {
    Node *node = (Node *) malloc(sizeof(Node));
    node->_next = NULL;
    node->name = name;

    return node;
}

CList *createCircularList() {

    CList *list = (CList *) malloc(sizeof(CList));
    list->length = 0;

    list->_head = NULL;
    list->_tail = NULL;

    return list;
}

Node * append(char *name, CList *list) {

    Node *node = createNode(name);

    if (list->_tail != NULL) {
        list->_tail->_next = node;
    }
    list->_tail = node;

    if (list->_head == NULL) {
        list->_head = node;
    }

    list->length++;
    node->_next = list->_head;

    return node;
}

void removeNode(Node *node, CList *list) {

    if (list->length == 1) {
        list->length = 0;
        list->_head = NULL;
        list->_tail = NULL;
        return;
    }

    Node *nextNode = node->_next;

    // Temporary assignment until we find the real previous node
    Node *prevNode = node;
    while (prevNode->_next != node) {
        prevNode = prevNode->_next;
    }
    prevNode->_next = nextNode;

    list->length--;
}


int getSize(CList *list) {
    return list->length;
}


int isEmpty(CList *list) {
    return getSize(list) == 0;
}


Node *getHead(CList *list) {
    return list->_head;
}

void destroyList(CList * list) {
    Node * currentNode = list->_head;
    Node * nextNode;

    while(list->length != 0){
        nextNode = currentNode->_next;
        destroyNode(currentNode);
        currentNode = nextNode;
        list->length--;
    }

    free(list);
}

void destroyNode(Node * node) {
    free(node);
}