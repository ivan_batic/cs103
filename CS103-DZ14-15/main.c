#include <stdio.h>
#include <stdlib.h>
#include "CircularList.h"

int main(int argc, char **argv) {

    CList *roundTable = createCircularList();

    append("Billie", roundTable);
    append("Jean", roundTable);
    append("Joe", roundTable);
    append("Joel", roundTable);
    append("Shaniqua", roundTable);
    append("Lakeisha", roundTable);


    Node *currentNode = getHead(roundTable);
    Node *nextNode = NULL;

    if (argc != 2) {
        printf("Usage:\n\t%s <stepSize>\n\n", argv[0]);
        exit(1);
    }

    int stepSize = atoi(argv[1]);
    if (stepSize < 1) {
        printf("You can't have a step size less than 1\n");
        exit(1);
    }

    printf("Original Order:\n");
    do {
        printf("\t%s\n", currentNode->name);
        currentNode = currentNode->_next;
    } while (currentNode != getHead(roundTable));

    printf("\nRemoval with step size of %d:\n", stepSize);
    currentNode = getHead(roundTable);
    FILE *fp = fopen("./izlaz.txt", "w");

    /**
     * This could be an O(1) operation if we had an indexed access to the nodes,
     * but that's not the proposition of the assignment. The list would have to
     * be implemented as an array optimized for reads and have more complex deletions with reindexing,
     * but that's not explicitly what the assignment is about, nor is the optimization goal specified.
     */
    while (!isEmpty(roundTable)) {
        for (int i = 0; i < stepSize - 1; i++) {
            currentNode = currentNode->_next;
        }

        // Outputting
        printf("\t%s\n", currentNode->name);
        fprintf(fp, "%s\n", currentNode->name);

        nextNode = currentNode->_next;

        removeNode(currentNode, roundTable);
        destroyNode(currentNode);

        currentNode = nextNode;
    }

    destroyList(roundTable);
    fclose(fp);

    return 0;
}